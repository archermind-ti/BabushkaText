## BabushkaText


### 项目介绍

自定义富文本组件，提供一种简单方法样式化Text，实现自定义文本各部分样式功能。

###  安装教程

方式一：

1. 下载模块代码添加到自己的工程

2. 关联使用

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       implementation project(':babushkatext')
       testImplementation 'junit:junit:4.13'
   	……
   }
   ```

4. gradle sync

方式二：

``` groovy
allprojects {
    repositories {
        mavenCentral()        
    }
}

...
dependencies {
	...
	implementation 'com.gitee.archermind-ti:BabushkaText:1.0.0'
	...
}
```

### 使用说明

按如下方式设置样式：

```
BabushkaText babushka = (BabushkaText)findComponentById(ResourceTable.Id_text);
        babushka.addPiece(new BabushkaText.Piece.Builder("Central Park, NY\n")
                .textColor(Color.getIntColor("#414141"))
                .build());
        babushka.addPiece(new BabushkaText.Piece.Builder("1.2 mi ")
                .textColor(Color.getIntColor("#0081E2"))
                .textSizeRelative(0.9f)
                .build());
        babushka.addPiece(new BabushkaText.Piece.Builder("from here")
                .textColor(Color.getIntColor("#969696"))
                .textSizeRelative(0.9f)
                .build());
        babushka.display();
```

支持的样式方法：

| Babushka 方法    |
| ---------------- |
| textSize         |
| textColor        |
| textSizeRelative |
| backgroundColor  |
| style            |
| underline        |
| strike           |
| superscript      |
| subscript        |

一旦创建并显示了BabushkaText，可以分别修改每个Piece的文本内容。 例如，如果要将上述示例中的1.2 mi更改为1.9 km，则可以执行以下操作：

```
// grab the Piece at position 1
Piece piece = babushka.getPiece(1);

// modify it's text
piece.setText("1.9 km ");

// you must always call display after you alter a Piece's text
babushka.display();
```



### 效果演示

<img src="/picture/sample.png" style="zoom:50%;" />



###  版本迭代

- v1.0.0

License
-------

    Copyright 2014 Henrique Boregio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.