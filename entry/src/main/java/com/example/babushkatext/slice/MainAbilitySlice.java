package com.example.babushkatext.slice;

import com.babushkatext.BabushkaText;
import com.example.babushkatext.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;


import ohos.agp.components.Component;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;

public class MainAbilitySlice extends AbilitySlice {
    private BabushkaText babushka1;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        // Display "9.5 excellent!"
        babushka1 = (BabushkaText)findComponentById(ResourceTable.Id_text);
        babushka1.addPiece(new BabushkaText.Piece.Builder("  9.5  ")
                .backgroundColor(Color.getIntColor("#073680"))
                .textColor(Color.WHITE.getValue())
                .build());
        babushka1.addPiece(new BabushkaText.Piece.Builder(" excellent! ")
                .backgroundColor(Color.getIntColor("#DFF1FE"))
                .textColor(Color.getIntColor("#073680"))
                .style(Font.DEFAULT_BOLD)
                .build());
        babushka1.display();

        // Display "3.4 horrible!"
        BabushkaText babushka2 = (BabushkaText)findComponentById(ResourceTable.Id_text2);
        babushka2.addPiece(new BabushkaText.Piece.Builder("  3.4  ")
                .backgroundColor(Color.getIntColor("#800736"))
                .textColor(Color.WHITE.getValue())
                .build());
        babushka2.addPiece(new BabushkaText.Piece.Builder(" horrible! ")
                .backgroundColor(Color.getIntColor("#fedfe2"))
                .textColor(Color.getIntColor("#800736"))
                .style(Font.DEFAULT_BOLD)
                .build());
        babushka2.display();

        // Display "starting at $420"
        BabushkaText babushka3 = (BabushkaText)findComponentById(ResourceTable.Id_text3);
        babushka3.addPiece(new BabushkaText.Piece.Builder("starting at ")
                .textColor(Color.getIntColor("#50AF2C"))
                .build());
        babushka3.addPiece(new BabushkaText.Piece.Builder("$420!")
                .textColor(Color.getIntColor("#50AF2C"))
                .textSizeRelative(1.2f)
                .style(Font.DEFAULT_BOLD)
                .build());
        babushka3.display();

        // Display "nightly price"
        BabushkaText babushka4 = (BabushkaText)findComponentById(ResourceTable.Id_text4);
        babushka4.addPiece(new BabushkaText.Piece.Builder("nightly price  ")
                .textColor(Color.getIntColor("#5F5F5F"))
                .superscript()
                .textSizeRelative(0.9f)
                .style(Font.DEFAULT_BOLD)
                .build());
        babushka4.addPiece(new BabushkaText.Piece.Builder("$256")
                .textColor(Color.getIntColor("#5F5F5F"))
                .superscript()
                .strike()
                .textSizeRelative(0.9f)
                .style(Font.DEFAULT_BOLD)
                .build());
        babushka4.addPiece(new BabushkaText.Piece.Builder(" $179")
                .textColor(Color.getIntColor("#9E0719"))
                .textSizeRelative(1.5f)
                .style(Font.DEFAULT_BOLD)
                .build());
        babushka4.display();

        // Display "new York"
        BabushkaText babushka5 = (BabushkaText)findComponentById(ResourceTable.Id_text5);
        babushka5.addPiece(new BabushkaText.Piece.Builder("New York, United States\n")
                .textColor(Color.getIntColor("#414141"))
                .style(Font.DEFAULT_BOLD)
                .build());
        babushka5.addPiece(new BabushkaText.Piece.Builder("870 7th Av, New York, Ny\n")
                .textColor(Color.getIntColor("#969696"))
                .textSizeRelative(0.9f)
                .style(Font.DEFAULT_BOLD)
                .build());
        babushka5.addPiece(new BabushkaText.Piece.Builder("10019, United States of America")
                .textColor(Color.getIntColor("#969696"))
                .textSizeRelative(0.8f)
                .build());
        babushka5.display();

        // Display "Central Park"
        BabushkaText babushka6 = (BabushkaText)findComponentById(ResourceTable.Id_text6);
        babushka6.addPiece(new BabushkaText.Piece.Builder("Central Park, NY\n")
                .textColor(Color.getIntColor("#414141"))
                .build());
        babushka6.addPiece(new BabushkaText.Piece.Builder("1.2 mi ")
                .textColor(Color.getIntColor("#0081E2"))
                .textSizeRelative(0.9f)
                .build());
        babushka6.addPiece(new BabushkaText.Piece.Builder("from here")
                .textColor(Color.getIntColor("#969696"))
                .textSizeRelative(0.9f)
                .build());
        babushka6.display();

        // Display "Bryant Park Hotel"
        BabushkaText babushka7 = (BabushkaText)findComponentById(ResourceTable.Id_text7);
        babushka7.addPiece(new BabushkaText.Piece.Builder("The Bryant Park Hotel\n")
                .textColor(Color.getIntColor("#414141"))
                .build());
        babushka7.addPiece(new BabushkaText.Piece.Builder("#6 of 434 ")
                .textColor(Color.getIntColor("#0081E2"))
                .build());
        babushka7.addPiece(new BabushkaText.Piece.Builder("in New York City\n")
                .textColor(Color.getIntColor("#969696"))
                .build());
        babushka7.addPiece(new BabushkaText.Piece.Builder("2487 reviews\n")
                .textColor(Color.getIntColor("#969696"))
                .build());
        babushka7.addPiece(new BabushkaText.Piece.Builder("$540")
                .textColor(Color.getIntColor("#F7B53F"))
                .style(Font.DEFAULT_BOLD)
                .build());
        babushka7.addPiece(new BabushkaText.Piece.Builder(" per night")
                .textColor(Color.getIntColor("#969696"))
                .build());
        babushka7.display();

        findComponentById(ResourceTable.Id_button).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                BabushkaText.Piece aPiece = babushka1.getPiece(0);
                aPiece.setText("  9.9  ");
                babushka1.display();
            }
        });


    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
