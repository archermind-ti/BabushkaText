package com.babushkatext;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class BabushkaText extends DirectionalLayout {

    // some default params
    private static int DEFAULT_ABSOLUTE_TEXT_SIZE;
    private static int DEFAULT_RELATIVE_TEXT_SIZE = 1;

    private List<Piece> mPieces;
    private DirectionalLayout mLayout;

    /**
     * Create a new instance of a this class
     * @param context
     */
    public BabushkaText(Context context) {
        super(context);
        init(context);
    }

    public BabushkaText(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(context);
    }

    public BabushkaText(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context);
    }

    private void init(Context context) {
        mPieces = new ArrayList<>();
        BabushkaText.DEFAULT_ABSOLUTE_TEXT_SIZE = (new Text(context)).getTextSize();
        setOrientation(Component.VERTICAL);
    }

    /**
     * Use this method to add a Piece to a BabushkaText.
     * Each Piece is added sequentially, so the
     * order you call this method matters.
     *
     * @param aPiece the Piece
     */
    public void addPiece(Piece aPiece) {
        mPieces.add(aPiece);
    }

    /**
     * Adds a Piece at this specific location. The underlying data structure is a
     * {@link java.util.List}, so expect the same type of behaviour.
     *
     * @param aPiece the Piece to add.
     * @param location the index at which to add.
     */
    public void addPiece(Piece aPiece, int location) {
        mPieces.add(location, aPiece);
    }

    /**
     * Replaces the Piece at the specified location with this new Piece. The underlying data
     * structure is a {@link java.util.List}, so expect the same type of behaviour.
     *
     * @param newPiece the Piece to insert.
     * @param location the index at which to insert.
     */
    public void replacePieceAt(int location, Piece newPiece) {
        mPieces.set(location, newPiece);
    }

    /**
     * Removes the Piece at this specified location. The underlying data structure is a
     * {@link java.util.List}, so expect the same type of behaviour.
     *
     * @param location the index of the Piece to remove
     */
    public void removePiece(int location) {
        mPieces.remove(location);
    }

    /**
     * Get a specific Piece in position index.
     *
     * @param location position of Piece (0 based)
     * @return Piece o null if invalid index
     */
    public Piece getPiece(int location) {
        if(location >= 0 && location < mPieces.size()) {
            return mPieces.get(location);
        }

        return null;
    }

    /**
     * Call this method when you're done adding Pieces
     * and want this TextView to display the final, styled version of it's String contents.
     *
     * You MUST also call this method whenever you make a modification to the text of a Piece that
     * has already been displayed.
     */
    public void display() {
        removeAllComponents();
        mLayout = newTextLayout();
        for (Piece aPiece : mPieces) {
            applySpannablesTo(aPiece);
        }
        addComponent(mLayout);
    }

    private void applySpannablesTo(Piece aPiece) {
        String[] strArr = aPiece.text.split("\\n", -1);

        for (int i=0; i<strArr.length; i++) {
            if (i > 0) {
                addComponent(mLayout);
                mLayout = newTextLayout();
            }
            mLayout.addComponent(newText(aPiece, strArr[i]));
        }
    }

    private Text newText(Piece aPiece, String str) {
        Text text = new Text(getContext());

        TextForm textForm = new TextForm();
        textForm.setTextColor(aPiece.textColor)
                .setRelativeTextSize(aPiece.textSizeRelative)
                .setSuperscript(aPiece.superscript)
                .setTextFont(aPiece.style)
                .setStrikethrough(aPiece.strike)
                .setUnderline(aPiece.underline)
                .setSubscript(aPiece.subscript);

        RichTextBuilder builder = new RichTextBuilder(textForm);
        String newStr = str.replace(" ", "\b");
        builder.addText(newStr);
        RichText richText = builder.build();

        text.setRichText(richText);
        ShapeElement bgElement1 = new ShapeElement();
        bgElement1.setRgbColor(RgbColor.fromArgbInt(aPiece.backgroundColor));
        text.setBackground(bgElement1);

        return text;
    }

    private DirectionalLayout newTextLayout() {
        DirectionalLayout layout = new DirectionalLayout(getContext());
        LayoutConfig layoutConfig = new LayoutConfig(LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        layout.setLayoutConfig(layoutConfig);
        layout.setOrientation(Component.HORIZONTAL);
        return layout;
    }

    /**
     * Resets the styling of this view and sets it's content to an empty String.
     */
    public void reset() {
        mPieces = new ArrayList<>();
        removeAllComponents();
    }

    /**
     * Change text color of all pieces of textview.
     */
    public void changeTextColor(int textColor) {
        for (Piece mPiece : mPieces) {
            mPiece.setTextColor(textColor);
        }
        display();
    }

    /**
     * A Piece represents a part of the text that you want to style. Say for example you want this
     * BabushkaText to display "Hello World" such that "Hello" is displayed in Bold and "World" is
     * displayed in Italics. Since these have different styles, they are both separate Pieces.
     *
     * You create a Piece by using it's Piece.Builder}
     *
     */
    public static class Piece {
        private String text;
        private int textColor;
        private final int textSize;
        private final int backgroundColor;
        private final float textSizeRelative;
        private final Font style;
        private final boolean underline;
        private final boolean superscript;
        private final boolean strike;
        private final boolean subscript;

        public Piece(Builder builder) {
            this.text = builder.text;
            this.textSize = builder.textSize;
            this.textColor = builder.textColor;
            this.backgroundColor = builder.backgroundColor;
            this.textSizeRelative = builder.textSizeRelative;
            this.style = builder.style;
            this.underline = builder.underline;
            this.superscript = builder.superscript;
            this.subscript = builder.subscript;
            this.strike = builder.strike;
        }

        public void setText(String text) {
            this.text = text;
        }

        public void setTextColor(int textColor) {
            this.textColor = textColor;
        }

        /**
         * Builder of Pieces
         */
        public static class Builder {
            // required
            private final String text;

            // optional
            private int textSize = DEFAULT_ABSOLUTE_TEXT_SIZE;
            private int textColor = Color.BLACK.getValue();
            private int backgroundColor = -1;
            private float textSizeRelative = DEFAULT_RELATIVE_TEXT_SIZE;
            private Font style = Font.DEFAULT;
            private boolean underline = false;
            private boolean strike = false;
            private boolean superscript = false;
            private boolean subscript = false;

            /**
             * Creates a new Builder for this Piece.
             *
             * @param text the text of this Piece
             */
            public Builder(String text) {
                this.text = text;
            }

            /**
             * Sets the absolute text size.
             *
             * @param textSize text size in pixels
             * @return a Builder
             */
            public Builder textSize(int textSize) {
                this.textSize = textSize;
                return this;
            }

            /**
             * Sets the text color.
             *
             * @param textColor the color
             * @return a Builder
             */
            public Builder textColor(int textColor) {
                this.textColor = textColor;
                return this;
            }

            /**
             * Sets the background color.
             *
             * @param backgroundColor the color
             * @return a Builder
             */
            public Builder backgroundColor(int backgroundColor) {
                this.backgroundColor = backgroundColor;
                return this;
            }

            /**
             * Sets the relative text size.
             *
             * @param textSizeRelative relative text size
             * @return a Builder
             */
            public Builder textSizeRelative(float textSizeRelative) {
                this.textSizeRelative = textSizeRelative;
                return this;
            }

            /**
             * Sets a style to this Piece.
             *
             * @param style Font style
             * @return a Builder
             */
            public Builder style(Font style) {
                this.style = style;
                return this;
            }

            /**
             * Underlines this Piece.
             *
             * @return a Builder
             */
            public Builder underline() {
                this.underline = true;
                return this;
            }

            /**
             * Strikes this Piece.
             *
             * @return a Builder
             */
            public Builder strike() {
                this.strike = true;
                return this;
            }

            /**
             * Sets this Piece as a superscript.
             *
             * @return a Builder
             */
            public Builder superscript() {
                this.superscript = true;
                return this;
            }

            /**
             * Sets this Piece as a subscript.
             *
             * @return a Builder
             */
            public Builder subscript() {
                this.subscript = true;
                return this;
            }

            /**
             * Creates a Piece with the customized
             * parameters.
             *
             * @return a Piece
             */
            public Piece build() {
                return new Piece(this);
            }

        }
    }
}
